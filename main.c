#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <windows.h>
#include <Shlwapi.h> //Shlwapi.lib
//#include <fileapi.h>
//Compile with kernel32
#include <errno.h>


#include "sqlite3.h"

#include "createdb.h"
#include "filecompare.h"

#define PROGVERSION "0.01.00"

typedef struct dbincluder {
    sqlite3 *db;
    void *data;
    void *data2;
} dbinc;

DWORD EnterFiles(char *szDir, char *theDir, char **fextarr, int recurs, sqlite3 *db, sqlite3_stmt *insstmt);

int streq_(const char *a, const char *b)
{
  if (!a || !b) return 0;
  unsigned long la, lb;
  la = strlen(a);
  lb = strlen(b);
  if (la != lb) return 0;
  if (memcmp(a,b,la)==0) return 1; else return 0;
}

int endeq_(char *a, char *b, int casesensitive)
{
  unsigned long la, lb, oa=0, ob=0, i, j;
  la = strlen(a);
  lb = strlen(b);
  if (la > lb) oa = la - lb;
  if (lb > la) ob = lb - la;
  j = ob;
  for (i=oa; i<la; i++)
  {
    if (a[i] != b[j] && (casesensitive || (toupper(a[i]) != toupper(b[j])))) return 0;
        j++;
  }
  return 1;
}

long FindChar(const char *astr, long maxlen, char fchar)
{
  long i;
  for (i=0;i<maxlen && astr[i]!=0;i++)
  {
    if (astr[i] == fchar) return i;
  }
  return -1;
}

int canopen(const char *afn)
{
    FILE *af = fopen(afn, "rb");
    if (!af)
    {
        fclose(af);
        return 0;
    }
    fclose(af);
    return 1;
}

char **splitfext(char *fextlist)
{
    int i, n=1,j;
    for (i=0;fextlist[i]!=0;i++)
    {
        if (fextlist[i]==',')  n++;
    }
    char **ans = (char **) malloc(sizeof(char *) * (n+1));
    char *pch = strtok(fextlist,",");
    for (i=0;i<n;i++)
    {
        if (pch != NULL)
        {
            ans[i] = strdup(pch);
            if (ans[i] == NULL)
            {
                for (j=0;j<i;j++) free(ans[j]);
                free(ans);
                return NULL;
            }
            pch = strtok(NULL,",");
        }
        else ans[i] = NULL;
    }
    ans[n] = NULL;
    return ans;
}

char *quoteexts(char **fextarr)
{
    int i, n=0;
    char *ans;
    for (i=0;fextarr[i]!=NULL;i++) n+=strlen(fextarr[i]);
    n = n<<2;
    n++;
    ans = (char *) malloc(sizeof(char)*n);
    if (!ans) return NULL;
    ans[0] = 0;
    for (i=0;fextarr[i]!=NULL;i++)
    {
        if (i>0)strcat(ans,",");
        strcat(ans,"'");
        strcat(ans,fextarr[i]);
        strcat(ans,"'");
    }
    return ans;
}

void freentsa(char **ntsa)
{
    int j;
    for (j=0;ntsa[j]!=NULL;j++) free(ntsa[j]);
    free(ntsa);
}

int select3cb(void *arg, int cols, char **coldata, char **colnames)
{
    //If this is called, abort!
    return 1;
}

int select2cb(void *arg, int cols, char **coldata, char **colnames)
{
    if (cols < 2) return 1;
    char *Path, *Size, *Duplicated = NULL, *asql, *tpath, *targ, *errs = NULL;
    dbinc *thedbinc = arg;
    sqlite3_stmt *updatedupstmt;
    int i, ret;
    for (i=0;i<cols;i++)
    {
      if (streq_(colnames[i], "Path")) Path = coldata[i];
      if (streq_(colnames[i], "Size")) Size = coldata[i];
      if (streq_(colnames[i], "Duplicated")) Duplicated = coldata[i];
    }
    i = IsFileSame((char *) thedbinc->data, Path);
    //if (Duplicated) printf("  -- '%s' Duplicate of '%s' (%d)\n'%s' is '%s'?\n",Path, Duplicated, (int) streq_(Duplicated,(char *) thedbinc->data),Duplicated,(char *) thedbinc->data);
    if (i>0)
    {
        //tpath = dbencodestring(Path);
        //if (!tpath) return 2;
        targ = dbencodestring((char *) thedbinc->data);
        if (!targ)
        {
            //free(tpath);
            return 2;
        }
        asql = (char *) malloc(sizeof(char)*(255+strlen(Size)+strlen(tpath)));
        if (!asql)
        {
            //free(tpath);
            free(targ);
            return 2;
        }
        snprintf(asql, (255+strlen(Size)/*+strlen(tpath)*/+strlen(targ)), "SELECT * FROM Files WHERE Path IS '%s' AND Duplicated IS NOT NULL ORDER BY Path;",targ);// OR Duplicated IS '%s' ORDER BY Path;", targ, tpath);
        ret=sqlite3_exec(thedbinc->db, asql, select3cb, arg, NULL);
        //free(tpath);
        free(targ);
        free(asql);
    /*} 
    if (i > 0 && */ if ((ret == SQLITE_DONE || ret == SQLITE_OK))// && !streq_(Duplicated,(char *) thedbinc->data)) //Dunno if this logic works?
    {
        updatedupstmt = (sqlite3_stmt *) thedbinc->data2;
        //Bind and run this.
        sqlite3_bind_text(updatedupstmt,1,(char *) thedbinc->data,strlen((char *) thedbinc->data),SQLITE_TRANSIENT); //SET Duplicated = $Arg
        sqlite3_bind_text(updatedupstmt,2,Path,strlen(Path),SQLITE_TRANSIENT);                                       //WHERE Path = $Path
        sqlite3_step(updatedupstmt);
        sqlite3_reset(updatedupstmt);
        
     }
    }
    return 0;
}

int select1cb(void *arg, int cols, char **coldata, char **colnames)
{
    if (cols < 2) return 1;
    char *Path, *Size, *asql, *errs = NULL, *tpath;
    int i;
    dbinc *thedbinc, *arg2;
    thedbinc = (dbinc *) arg;
    for (i=0;i<cols;i++)
    {
      if (streq_(colnames[i], "Pa")) Path = coldata[i];
      if (streq_(colnames[i], "Si")) Size = coldata[i];
    }
    arg2 = (dbinc *) malloc(sizeof(dbinc));
    if (!arg2)
    {
        fprintf(stderr, "Error: Out of memory!\n");
        return 2;
    }
    arg2->db = thedbinc->db;
    arg2->data = strdup(Path);
    arg2->data2 = thedbinc->data2;
    asql = (char *)malloc(sizeof(char)*(256+strlen(Size)+(2*strlen(Path))));
    if (!asql)
    {
        fprintf(stderr, "Error: Out of memory!\n");
        free(arg2->data);
        free(arg2);
        return 2;
    }
    tpath = dbencodestring(Path);
    if (!tpath)
    {
        fprintf(stderr, "Error: Out of memory!\n");
        free(asql);
        free(arg2->data);
        free(arg2);
        return 2;
    }
    snprintf(asql, (255+strlen(Size)+strlen(tpath)), "SELECT * FROM Files WHERE Size IS %s AND Path IS NOT '%s' ORDER BY Path;", Size, tpath);//AND Duplicated IS NULL 
    i = sqlite3_exec(thedbinc->db, asql, select2cb, arg2, &errs);
    if (i!=SQLITE_DONE && i!=SQLITE_OK)
    {
        if (errs)
        {
            fprintf(stderr, "Error querying database for update: %s\n", errs);
            sqlite3_free(errs);
            free(tpath);
            free(asql);
            free(arg2->data);
            free(arg2);
        }
        return 3;
    }
    free(tpath);
    free(asql);
    free(arg2->data);
    free(arg2);
    return 0;
}

int selectrepcb(void *arg, int cols, char **coldata, char **colnames)
{
    if (cols < 4) return 1;
    char *Path, *Size, *OPath, *OSize;//, *asql, *errs = NULL;
    char Reply[256] = "";
    int i, delflag, ret;
    delflag = (int) arg;
    //dbinc *thedbinc, *arg2;
    //thedbinc = (dbinc *) arg;
    for (i=0;i<cols;i++)
    {
      if (streq_(colnames[i], "Pa")) Path = coldata[i];
      if (streq_(colnames[i], "Si")) Size = coldata[i];
      if (streq_(colnames[i], "OPa")) OPath = coldata[i];
      if (streq_(colnames[i], "OSi")) OSize = coldata[i];
    }
    if (Path && Size && OPath) printf("%s (%s bytes) duplicates %s.\n", Path, Size, OPath);
    ret=0;
    if (delflag == 1)
    {
        printf("Do you want to delete \"%s\"? [Y/N]\n",Path);
        if (fgets(Reply, 255, stdin))
        {
            for (i=0;Reply[i]==' '&&Reply[i]=='\t';i++);
            if (toupper(Reply[i])=='Y') ret = 1;
        }
    }
    if (ret || delflag == 2)
    {
        //Delete Path
        if (_unlink(Path) == -1) perror("Error deleting file!\n");
        else fprintf(stdout,"Deleted %s!\n" ,Path);
    }
    return 0;
}

int main(int argc, char *argv[])
{
    sqlite3 *db;
    sqlite3_stmt *ins, *upddup;
    char *directory = NULL, *existingdb = NULL, *exts = NULL, *dbfilename;
    char **extsarr = NULL;
    int delflag = 0, keepflag = 0;
    int i, ret = 0;
    char dbfile[512] = "./i4d";

    if (argc < 2 || streq_(argv[1],"/?") || streq_(argv[1],"-?") || streq_(argv[1],"--help") )
    {
        printf("DHeadshot's Software Creations IndexForDuplicates\nVersion: %s\n", PROGVERSION);
        printf("Creates an index database to search for duplicate files.\n");
        printf("Usage:\n\t%s [-?] [-O=ExistingDB] [-e=ExtensionList] [-d|-D] [-S] DirectoryPath\n",argv[0]);
        printf("Where:\n\t-?\tWrites this text\n");
        printf("\t-O=ExistingDB\tOpens an existing index database (here called ExistingDB) and checks that\n");
        printf("\t-e=ExtensionList\tLimits the indexing/checking to a list of file extensions of the form \"a,b,c,d\", "
               "e.g. -e=png,jpg,jpeg,gif,tif,tiff,bmp limits to common picture types.\n");
        printf("\t-d\tPrompts to delete any duplicates found.\n");
        printf("\t-D\tAutomatically deletes any duplicates found!\n");
        printf("\t-S\tDoes not delete the index database when the program ends.\n");
        printf("\tDirectoryPath\tThe path of the directory to search.\n");
        
        return 1;
    }
    
    for (i=1; i<argc;i++)
    {
        if (argv[i][0] == '-')
        {
            switch (argv[i][1])
            {
                case 'O':
                  if (argv[i][2]=='=') existingdb = argv[i] + 3;
                  else fprintf(stderr, "Error: Bad option \"%s\"!\n", argv[i]);
                break;

                case 'e':
                  if (argv[i][2]=='=') exts = argv[i] + 3;
                  else fprintf(stderr, "Error: Bad option \"%s\"!\n", argv[i]);
                break;

                case 'd':
                 if (!delflag) delflag = 1;
                break;

                case 'D':
                  delflag = 2;
                break;

                case 'S':
                  keepflag = 1;
                break;

                case '?':
                case 'h':
                  fprintf(stderr, "Error: \"-?\" option must be first!\n", argv[i]);
                break;

                default:
                  fprintf(stderr, "Error: Bad option \"%s\"!\n", argv[i]);
                break;
            }
        }
        else directory = argv[i];
    }

    if (existingdb)
    {
        dbfilename = strdup(existingdb);
        if (PathFileExistsA(existingdb)) ret = opendb(&db, &ins, &upddup, existingdb);
        if (!ret) fprintf(stderr, "Could not open database.\n");
    }
    else
    {
        char adbfile[518] = "";
        strcpy(adbfile,dbfile);
        strcat(adbfile,".db");
        while (PathFileExistsA(adbfile) && strlen(dbfile)<510)
        {
            strcat(dbfile,"_"); //Stupid hack, but does the job
            strcpy(adbfile,dbfile);
            strcat(adbfile,".db");
        }
        dbfilename = strdup(adbfile);
        ret = createdb(&db, &ins, &upddup, adbfile);
        if (!ret) fprintf(stderr, "Could not create database.\n");
        //free(adbfile);
    }
    if (!ret)
    {
        free(dbfilename);
        return 2;
    }

    if (exts)
    {
        extsarr = splitfext(exts);
        if (!extsarr)
        {
            fprintf(stderr, "Error: Out of memory!\n");
            sqlite3_finalize(ins);
            sqlite3_finalize(upddup);
            sqlite3_close(db);
            if (!keepflag && !existingdb)
            {
                if (_unlink(dbfilename) == -1) perror("Error deleting indexing database!\n");//, dbfilename);
                else printf("Deleted indexing database.\n");
            }
            free(dbfilename);
            return 3;
        }
    }

    //EnterFiles(char *szDir, char *theDir, char **fextarr, int recurs, sqlite3 *db, sqlite3_stmt *insstmt)
    if (!existingdb)
    {
        //Only index if we don't have a specified db
        char *szdir = strdup(directory);
        if (szdir) szdir = realloc(szdir, strlen(directory)+8);
        if (!szdir)
        {
            fprintf(stderr, "Error: Out of memory!\n");
            if (extsarr) freentsa(extsarr);
            sqlite3_finalize(ins);
            sqlite3_finalize(upddup);
            sqlite3_close(db);
            if (!keepflag && !existingdb)
            {
                if (_unlink(dbfilename) == -1) perror("Error deleting indexing database!\n");//, dbfilename);
                else printf("Deleted indexing database.\n");
            }
            free(dbfilename);
            return 3;
        }
        strcat(szdir, "\\*");
        printf("Indexing '%s'...\n", directory);
        ret = (int) EnterFiles(szdir, directory, extsarr, 1, db, ins);
        //Do something with ret?
    }
    
    char *errs;
    char *asql;
    
    if (exts) asql = (char *) malloc(sizeof(char)*(255+strlen(exts)));
    else asql = malloc(sizeof(char)*(255));
    if (!asql)
    {
        fprintf(stderr, "Error: Out of memory!\n");
        if (extsarr) freentsa(extsarr);
        sqlite3_finalize(ins);
        sqlite3_finalize(upddup);
        sqlite3_close(db);
        if (!keepflag && !existingdb)
        {
            if (_unlink(dbfilename) == -1) perror("Error deleting indexing database!\n");//, dbfilename);
            else printf("Deleted indexing database.\n");
        }
        free(dbfilename);
        return 3;
    }
    if (exts)
    {
        char *qexts = quoteexts(extsarr);
        if (!qexts)
        {
            fprintf(stderr, "Error: Out of memory!\n");
            free(asql);
            if (extsarr) freentsa(extsarr);
            sqlite3_finalize(ins);
            sqlite3_finalize(upddup);
            sqlite3_close(db);
            if (!keepflag && !existingdb)
            {
                if (_unlink(dbfilename) == -1) perror("Error deleting indexing database!\n");//, dbfilename);
                else printf("Deleted indexing database.\n");
            }
            free(dbfilename);
            return 3;
        }
        snprintf(asql, strlen(qexts)+255,
                       "CREATE VIEW IF NOT EXISTS Sizes AS SELECT Size, COUNT(Path) AS PCount FROM Files WHERE Locked IS 0 "
                       "AND Duplicated IS NULL AND Extension IN (%s) GROUP BY Size;",
                       qexts);
        free(qexts);
    }
    else strcpy(asql, "CREATE VIEW IF NOT EXISTS Sizes AS SELECT Size, COUNT(Path) AS PCount FROM Files WHERE Locked IS 0 "
                      " AND Duplicated IS NULL GROUP BY SIZE;");
    
    ret = sqlite3_exec(db, "DROP VIEW IF EXISTS Sizes;", NULL, NULL, &errs);
    if (ret != SQLITE_DONE && ret != SQLITE_OK)
    {
        fprintf(stderr, "Error querying database: %s!\n", errs);
        sqlite3_free(errs);
    }
    else
    {
        ret = sqlite3_exec(db, asql, NULL, NULL, &errs);
        if (ret != SQLITE_DONE && ret != SQLITE_OK)
        {
            fprintf(stderr, "Error querying database: %s!\n", errs);
            sqlite3_free(errs);
        }
    }
    if (ret != SQLITE_DONE && ret != SQLITE_OK)
    {
        if (extsarr) freentsa(extsarr);
        sqlite3_finalize(ins);
        sqlite3_finalize(upddup);
        sqlite3_close(db);
        if (!keepflag && !existingdb)
        {
            if (_unlink(dbfilename) == -1) perror("Error deleting indexing database!\n");//, dbfilename);
            else printf("Deleted indexing database.\n");
        }
        free(dbfilename);
        return 4;
    }
    free(asql);
    
    /*if (exts) asql = (char *) malloc(sizeof(char)*(255+strlen(exts)));
    else asql = malloc(sizeof(char)*(255));
    if (!asql)
    {
        fprintf(stderr, "Error: Out of memory!\n");
        if (extsarr) freentsa(extsarr);
        sqlite3_finalize(ins);
        sqlite3_finalize(upddup);
        sqlite3_close(db);
        if (!keepflag && !existingdb)
        {
            if (_unlink(dbfilename) == -1) perror("Error deleting indexing database '%s'!\n", dbfilename);
            else printf("Deleted indexing database.\n");
        }
        free(dbfilename);
        return 3;
    }
    if (exts) snprintf(asql, strlen(exts)+255, "SELECT Path, Size FROM Files WHERE Locked IS 0 "
                                               "AND Duplicated IS NULL AND Extension IN (%s)"
                                               "ORDER BY Size;",
                       exts);
    else strcpy(asql, "SELECT Path, Size FROM Files WHERE Locked IS 0 AND Duplicated IS NULL ORDER BY SIZE;"
    */
    
    dbinc adbi;
    adbi.db = db;
    adbi.data = NULL;
    adbi.data2 = upddup;
    ret = sqlite3_exec(db, "SELECT Files.Path AS Pa, Files.Size AS Si FROM Files INNER JOIN Sizes ON Files.Size = Sizes.Size WHERE Sizes.PCount > 1;",
                       select1cb, &adbi, &errs);
    if (ret != SQLITE_OK && ret != SQLITE_DONE)
    {
        fprintf(stderr, "Error querying database for update: %s!\n", errs);
        sqlite3_free(errs);
    }

    //Query DB for report
    printf("Report:\n=======\n");
    ret = sqlite3_exec(db, "SELECT Files.Path AS Pa, Files.Size as Si, Originals.Path AS OPa, Originals.Size AS OSi "
                           " FROM Files INNER JOIN (SELECT Path, Size FROM Files WHERE Duplicated IS NULL) AS Originals ON Files.Duplicated = Originals.Path "
                           " WHERE Files.Duplicated IS NOT NULL ORDER BY OPa ASC; ",
                       selectrepcb, (void *) delflag, &errs);
    if (ret != SQLITE_OK && ret != SQLITE_DONE)
    {
        fprintf(stderr, "Error querying database for report: %s!\n", errs);
        sqlite3_free(errs);
    }
    
    
    
    //End
    if (extsarr) freentsa(extsarr);
    sqlite3_finalize(ins);
    sqlite3_finalize(upddup);
    sqlite3_close(db);
    
    if (!keepflag)
    {
        if (_unlink(dbfilename) == -1)
        {
            perror("Error deleting indexing database!\n");//, dbfilename);
        }
        else printf("Deleted indexing database.\n");
    }
    free(dbfilename);
    return 0;
}

int doinsert(sqlite3 *db, sqlite3_stmt *insstmt, const char *filename, const char *extension, HANDLE hfile, DWORD fszl, DWORD fszh)
{
    LARGE_INTEGER FileSize ;
    BY_HANDLE_FILE_INFORMATION FileInformation;
    unsigned long long fsz;
    int ret;
    //Filename
    sqlite3_bind_text(insstmt,1,filename,strlen(filename),SQLITE_TRANSIENT);
    //Extension
    sqlite3_bind_text(insstmt,2,extension,strlen(extension),SQLITE_TRANSIENT);
    //Size
    //if (GetFileSizeEx(hfile, &FileSize))// == TRUE)
    //if (GetFileInformationByHandle(hfile, &FileInformation))
    if (fszl || fszh)
    {
        //fsz = ((unsigned long long)FileInformation.nFileSizeLow) + (((unsigned long long)FileInformation.nFileSizeHigh) <<32);
        fsz = ((unsigned long long)fszl) + (((unsigned long long)fszh) *(MAXDWORD+1));
        sqlite3_bind_int64(insstmt,3, (sqlite3_int64) fsz);//FileSize.QuadPart);
    }
    else
    {
        //fprintf(stderr, "  -- Can't get file size: %n.\n", GetLastError());
        fsz = 0;
        sqlite3_bind_null(insstmt,3);
    }
    //Locked from read
    ret = canopen(filename);
    if (ret) sqlite3_bind_int(insstmt,4,0);
    else  sqlite3_bind_int(insstmt,4,1);
    //Duplicated
    sqlite3_bind_null(insstmt,5);
    //    printf("  -- Inserting '%s'(.%s, %Ld) - %d\n",filename, extension, fsz, ret);//FileSize.QuadPart);
    ret = sqlite3_step(insstmt);
    if (ret != SQLITE_DONE && ret != SQLITE_OK)
    {
       fprintf(stderr, "Error indexing file '%s': %s\n", filename, sqlite3_errstr(ret));
    }
    sqlite3_reset(insstmt);
    return ret;
}

DWORD EnterFiles(char *szDir, char *theDir, char **fextarr, int recurs, sqlite3 *db, sqlite3_stmt *insstmt)
{
  HANDLE hFind = INVALID_HANDLE_VALUE;
  DWORD dwError=0;
  WIN32_FIND_DATA ffd;
  
  //This is not the fastest way of working.  Perhaps we should start a transaction then commit later?
  
  hFind = FindFirstFile(szDir, &ffd);
  char chDir[MAX_PATH *2], nchDir[MAX_PATH *2];
  
  if (INVALID_HANDLE_VALUE == hFind) 
  {
    fprintf(stderr,"Error finding file(s) in \"%s\".\n",theDir);
    dwError = GetLastError();
    return dwError;
  } 
  long ic = 0, oic=0;
  int changed = 0, i, ret;
  char afn[MAX_PATH] = "\0";
  LARGE_INTEGER FileSize ;
  do
  {
      if ((ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0)
      {
        if (fextarr)
        {
            for (i=0;fextarr[i]!=NULL;i++)
            {
                strcpy(afn,".");
                strcat(afn, fextarr[i]);
                if (endeq_(afn, ffd.cFileName, 0))
                {
                    //printf("%s, %s\n",ffd.cFileName, theDir);
                    /*
                    sqlite3_bind_text(insstmt,1,ffd.cFileName,strlen(ffd.cFileName),SQLITE_TRANSIENT);
                    sqlite3_bind_text(insstmt,2,afn,strlen(afn),SQLITE_TRANSIENT);
                    if (GetFileSizeEx(hFind, &FileSize) == TRUE)
                    {
                        sqlite3_bind_int64(insstmt,3, (sqlite3_int64) FileSize.QuadPart);
                    }
                    else
                    {
                        sqlite3_bind_null(insstmt,3);
                    }
                    //Locked from read
                    if (canopen(ffd.cFileName)) sqlite3_bind_int(insstmt,4,0);
                    else  sqlite3_bind_int(insstmt,4,1);
                    sqlite3_bind_null(insstmt,5);
                    ret = sqlite3_step(insstmt);
                    if (ret != SQLITE_DONE && ret != SQLITE_OK)
                    {
                        fprintf(stderr, "Error indexing file '%s': %s\n", ffd.cFileName, sqlite3_errstr(ret));
                    }
                    sqlite3_reset(insstmt);
                    */
                    //int doinsert(sqlite3 *db, sqlite3_stmt *insstmt, const char *filename, const char *extension, HANDLE hfile)
                    strcpy(chDir,theDir);
                    strcat(chDir,"\\");
                    strcat(chDir, ffd.cFileName);
                    
                    ret = doinsert(db, insstmt, chDir, fextarr[i], hFind, ffd.nFileSizeLow, ffd.nFileSizeHigh);
                    //Maybe do something with ret?
                }
            }
        }
        else
        {
            //printf("%s, %s\n",ffd.cFileName, theDir);
            //    printf("  -- Indexing %s\n",ffd.cFileName);
            //FindChar(char *astr, long maxlen, char fchar)
            ic=0;
            while (ic != -1)
            {
                oic = ic;
                if (ic) ic = FindChar(ffd.cFileName+ic+1, strlen(ffd.cFileName), '.');
                else ic = FindChar(ffd.cFileName, strlen(ffd.cFileName), '.');
            }
            strcpy(chDir,theDir);
            strcat(chDir,"\\");
            strcat(chDir, ffd.cFileName);
            
            if (oic > 0)
            {
                ret = doinsert(db, insstmt, chDir, (const char *) (ffd.cFileName + oic+1), hFind, ffd.nFileSizeLow, ffd.nFileSizeHigh);
            }
            else
            {
                ret = doinsert(db, insstmt, chDir, (const char *) "", hFind, ffd.nFileSizeLow, ffd.nFileSizeHigh);
            }
            //Maybe do something with ret?
        }
      }
        
      if (recurs && (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) && !streq_(ffd.cFileName,".") && !streq_(ffd.cFileName,".."))
      {
          strcpy(chDir,theDir);
          strcat(chDir,"\\");
          strcat(chDir, ffd.cFileName);
          strcpy(nchDir,chDir);
          strcat(nchDir,"\\*");
          dwError = EnterFiles(nchDir,chDir, fextarr, recurs, db, insstmt);
          if (dwError != ERROR_NO_MORE_FILES) 
          {
            fprintf(stderr, "Error recursing in %s!\n",chDir);
          }
      }
  } while (FindNextFile(hFind, &ffd) != 0);
  dwError = GetLastError();
  if (dwError != ERROR_NO_MORE_FILES) 
  {
     fprintf(stderr, "Error indexing files!\n");
  }
  
  FindClose(hFind);
  return dwError;
}
